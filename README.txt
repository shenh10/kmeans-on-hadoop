数据集中一共包括以下文件和文件夹：
1. 文件夹：RenameImageset 包括所有的数据集图像，图像名称为“数字.jpg”，一共2152张图像，分为17个，图像名称与类别无关，图像为从Flickr上爬虫得到，每一类别具有相同的Flickr标签。
2. 文件：all_sift_data_rename.txt：包括所有的SIFT点数据集，文件格式已经在实验要求中给出。
3. 文件：ExtractFeat.java: 抽取文件2中每一行SIFT点的示例程序，不保证性能一定最佳，仅作为参考使用。 