import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Map;    
import java.util.HashMap;   
import java.math.BigDecimal;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.util.GenericOptionsParser;




public class KMeans{
     public static class ClusterVector implements  WritableComparable<ClusterVector>{
       // private int index ;
         private int length = 0;
        private int label;
        public double[] vector;
        private  double error = 10;
        public ClusterVector(){
                super();

        }
        public ClusterVector(int length, int label,double[] vector) {

                super();
                this.length = length;
                this.label = label;
            this.vector = vector;
        }
        
        public double[] getVector() {
            return vector;
        }
        public void setVector(double[] vector) {
            this.vector = vector;
        }
    
          public int getLabel() {
            return label;
        }
        public void setLabel(int label) {
            this.label = label;
        }
          public double getLength() {
            return length;
        }
        public void setLength(int length) {
            this.length = length;
        }
        public void add(ClusterVector b){
            for (int i = 0; i< this.length; i++)
            {
                this.vector[i] += b.vector[i];
            }
        }
   
        public String ToString(){
            String str = "";
            for(int i = 0; i< this.length;i++){
                str = str + this.vector[i]+' ';
            }
            return str;
        }
        public int caculateErr(ClusterVector a){
            double err = 0;
            for(int i=0; i< this.length;i++)
            {
                err += Math.abs(a.vector[i]-this.vector[i]);
            }
		System.out.println("Error:"+err);
            return err>error? 1:0;
        }
        public void divide(double divider){
            for (int i = 0; i< this.length; i++)
            this.vector[i] = round(this.vector[i]/divider, 5, BigDecimal.ROUND_CEILING);

        }
         @Override
          public final void readFields(DataInput in) throws IOException {
            this.length = in.readInt();
            this.label = in.readInt();
            this.vector = new double[this.length];
            for (int i = 0; i< this.length;i++)
                this.vector[i] = in.readDouble();
            }
             @Override
          public final void write(DataOutput out) throws IOException {
            
            out.writeInt(this.length);
            out.writeInt(this.label);
             for (int i = 0; i< this.length;i++)
                 out.writeDouble(this.vector[i]);
          }
        @Override
        public final int compareTo(ClusterVector o) {
            if (this.length != o.getLength() )
                return (this.length<o.getLength()? -1:1);
            if(this.label!= o.getLabel())
                return (this.label<o.getLabel()? -1:1);
            for(int i = 0; i< this.getLength(); i++){
                if(this.vector[i]!=o.vector[i])
                {
                    return (this.vector[i]<o.vector[i]? -1:1);
                }
            }

            return 0;
        }
          @Override
          public final boolean equals(Object obj) {
            if (this == obj)
              return true;
            if (obj == null)
              return false;
            if (getClass() != obj.getClass())
              return false;
            ClusterVector other = (ClusterVector) obj;
            if (vector == null) {
              if (other.vector != null)
                return false;
            } else if(vector.length!=other.vector.length)
              return false;
              else {
                for(int i=0; i< vector.length;i++){
                    if(vector[i]!=other.vector[i])
                        return false;
                }
              }
            return true;
          }


     }
 
     private static double ComputeDistDouble(double[] featArray, double[] cent){
        double sum = 0;
    
        for(int i = 0; i<featArray.length; i++)
        {
            sum += Math.pow((featArray[i] - cent[i]),2);
        }
        return sum;
    }
     public static double round(double value, int scale, int roundingMode) {  
        BigDecimal bd = new BigDecimal(value);  
        bd = bd.setScale(scale, roundingMode);  
        double d = bd.doubleValue();  
        bd = null;  
        return d;  
    }  
   
    public static class SIFTMapper extends Mapper<Object, Text, ClusterVector, Text>
    {	    
      
	private static final Log LOG = LogFactory.getLog(SIFTMapper.class);
        ClusterVector[] centers;
        @Override
        protected  void setup(Context context) throws IOException,
          InterruptedException {
            super.setup(context);
            Configuration conf = context.getConfiguration();
            int featureNum = Integer.parseInt(conf.get("featureNum"));
            centers = new ClusterVector[featureNum];
            String filePath = conf.get("centroid.path")+conf.get("iteration");
            String temp = "";
            String[] stringArray;
            double[] featArray;
            int index = 0;
            
              BufferedReader bReader = null;
            FileSystem fs = FileSystem.get(conf);
            Path file = new Path(filePath);
            try{
            bReader = new BufferedReader(new InputStreamReader(fs.open(file)));
            }catch(Exception e){
                e.printStackTrace();
            }  
            try{
                while ((temp = bReader.readLine()) != null && index < featureNum) {
                    stringArray = temp.split(" ");
                    featArray = new double[stringArray.length - 2];
                    for(int i = 2; i < stringArray.length; i++){
                        featArray[i - 2] = Double.parseDouble(stringArray[i]);
                    }
                    centers[index]  = new ClusterVector(featArray.length, index, featArray);
                    index++;
                }
		bReader.close();
            }catch (Exception e) {
                 e.printStackTrace();
            }
      }
        @Override
        public  void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {  
            
            String[] stringArray; 
            double[] featArray;
            int imageID =0;
            int featureID = 0;
            //Get whole dimension of data
            stringArray = value.toString().split(" ");
                featArray = new double[stringArray.length - 2];
                imageID = Integer.parseInt(stringArray[0]);
                featureID = Integer.parseInt(stringArray[1]);
                for(int i = 2; i < stringArray.length; i++){
                    featArray[i - 2] = Double.parseDouble(stringArray[i]);
            }
		LOG.info("map:centersize"+centers.length);
            double minDist = Double.MAX_VALUE;
            double dst;
            int minIndex = 0;
            if(centers.length==0) return;
            for (int i = 0 ; i< centers.length; i++){
                dst = ComputeDistDouble(featArray,this.centers[i].getVector());
                if(dst<minDist)
                {
                    minIndex = i;
                    minDist = dst;
                }
            }
            context.write(centers[minIndex],value);
            return;
        }
    }

    public static  class SIFTReducer extends Reducer<ClusterVector,Text,IntWritable,IntWritable> 
    {

          public static enum Counter {
            CONVERGED
          }
        private IntWritable result = new IntWritable();
        private Text user_name = new Text();
         List<ClusterVector> centers = new ArrayList<>();
        public void reduce(ClusterVector key, Iterable<Text> values, Context context) throws IOException, InterruptedException 
        {
          
             int count = 0;
            String[] stringArray; 
            double[] featArray;
            int imageID =0;
            ClusterVector newCenter = new ClusterVector() ;
      
            for(Text value: values){
                //Get whole dimension of data
                stringArray = value.toString().split(" ");
                    featArray = new double[stringArray.length - 2];
            
                    imageID = Integer.parseInt(stringArray[0]);
                for(int i = 2; i < stringArray.length; i++){
                    featArray[i - 2] = Double.parseDouble(stringArray[i]);
                }
                if(count == 0){
                    newCenter.setLength(featArray.length);
                    newCenter.setLabel(0);
                    newCenter.setVector(featArray);

                }else
                    newCenter.add(new ClusterVector(featArray.length,0, featArray));
                     context.write(new IntWritable(centers.size()), new IntWritable(imageID));
                    count++;   

             }

            newCenter.divide(count);

            //System.out.println("#################Previous Center#####################");
           // System.out.println(key.ToString());
            // System.out.println("#################New Center#####################");
           // System.out.println(newCenter.ToString());
             centers.add(newCenter);
            if(newCenter.caculateErr(key) == 1)
                 context.getCounter(Counter.CONVERGED).increment(1);
        }
        @Override
          protected void cleanup(Context context) throws IOException,
              InterruptedException {
            super.cleanup(context);
            Configuration conf = context.getConfiguration();
            Path outPath = new Path(conf.get("centroid.path")+Integer.toString(Integer.parseInt(conf.get("iteration"))+1));
            FileSystem fs = FileSystem.get(conf);
          
            BufferedWriter bWriter = null;
            try{
                 bWriter = new BufferedWriter(new OutputStreamWriter(fs.create(outPath)));
            }catch(Exception e){
                e.printStackTrace();
            }  
            try{
                int number = 0;
                for (ClusterVector center:centers){
                    bWriter.write("0 "+number+" "+center.ToString());
                    bWriter.newLine();
                    number++;
                }
		bWriter.close();   
         }catch (Exception e) {
                 e.printStackTrace();
            }
    
        }
    }
       public static class BOWProducerMapper extends Mapper<Object, Text, IntWritable, IntWritable>{
         @Override
            public  void map(Object key, Text value, Context context) throws IOException, InterruptedException
            {
                String [] stringArray;
                stringArray = value.toString().split("\t");
                int cat = Integer.parseInt(stringArray[0]);
                context.write(new IntWritable(Integer.parseInt(stringArray[1])),new IntWritable(cat));
            }
     }
  

      public static  class BOWProducerReducer extends Reducer<IntWritable,IntWritable,IntWritable,Text> 
    {
        int featureNum;
        @Override
        protected  void setup(Context context) throws IOException,
          InterruptedException {
            super.setup(context);
            Configuration conf = context.getConfiguration();
            featureNum = Integer.parseInt(conf.get("featureNum"));
        }
      @Override
        public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException 
       {
            int[] hist = new int[featureNum];
            for(int i = 0; i< featureNum; i++){
                hist[i] = 0;
            }
            for(IntWritable value: values){
                hist[value.get()] ++;
            }
            String str = "";
            for(int i = 0; i< hist.length; i++)
                str = str +Integer.toString(hist[i])+ " ";
            context.write(key, new Text(str));
       }
    }
    public static class ClassifyMapper extends Mapper<Object, Text, ClusterVector, Text>
        {
            ClusterVector[] centers;
        @Override
        protected  void setup(Context context) throws IOException,
          InterruptedException {
            super.setup(context);
            Configuration conf = context.getConfiguration();
             int featureNum = Integer.parseInt(conf.get("featureNum"));
            centers = new ClusterVector[featureNum];
            String filePath = conf.get("centroid.path")+conf.get("iteration");
            String temp = "";
            String[] stringArray;
            double[] featArray;
            int index = 0;
              BufferedReader bReader = null;
            FileSystem fs = FileSystem.get(conf);
            Path file = new Path(filePath);
            try{
            bReader = new BufferedReader(new InputStreamReader(fs.open(file)));
            }catch(Exception e){
                e.printStackTrace();
            }  
            try{
                while ((temp = bReader.readLine()) != null && index < featureNum) {
                    stringArray = temp.split(" ");
                    featArray = new double[stringArray.length - 1];
                    for(int i = 1; i < stringArray.length; i++){

                        featArray[i - 1] = Double.parseDouble(stringArray[i]);
                    }
                    centers[index]  = new ClusterVector( featArray.length, Integer.parseInt(stringArray[0]),featArray);
                    index++;
                }
		bReader.close();
            }catch (Exception e) {
                 e.printStackTrace();
            }
        }
             @Override
            public  void map(Object key, Text value, Context context) throws IOException, InterruptedException
            {
                String[] stringArray; 
                double[] featArray;
                int label =0;
                //Get whole dimension of data
                stringArray = value.toString().split(" ");
                    featArray = new double[stringArray.length - 1];
                    label = Integer.parseInt(stringArray[0]);
                    for(int i = 1; i < stringArray.length; i++){
                        featArray[i - 1] = Double.parseDouble(stringArray[i]);
                }
                double minDist = Double.MAX_VALUE;
                double dst;
                int minIndex = 0;

                if(centers.length==0) return;
                for (int i = 0 ; i< centers.length; i++){
                    dst = ComputeDistDouble(featArray,this.centers[i].getVector());
                    if(dst<minDist)
                    {
                        minIndex = i;
                        minDist = dst;
                    }
                }
                    context.write(centers[minIndex],value);
                    return;
            } 
        }
    public static class ClassifyReducer extends Reducer<ClusterVector, Text, IntWritable, IntWritable>{
         public static enum Counter {
            CONVERGED
          }
         List<ClusterVector> centers = new ArrayList<>();
        public void reduce(ClusterVector key, Iterable<Text> values, Context context) throws IOException, InterruptedException 
        {
          
             int count = 0;
            String[] stringArray; 
            double[] featArray;
            int label =0;
            ClusterVector newCenter = new ClusterVector() ;
            for(Text value: values){
                //Get whole dimension of data
                stringArray = value.toString().split(" ");
                    featArray = new double[stringArray.length - 1];
            
                    label = Integer.parseInt(stringArray[0]);
                for(int i = 1; i < stringArray.length; i++){
                    featArray[i - 1] = Double.parseDouble(stringArray[i]);
                }
                if(count == 0){
                    newCenter.setLength(featArray.length);
                    newCenter.setLabel(0);
                    newCenter.setVector(featArray);
                }else
                    newCenter.add(new ClusterVector(featArray.length,label,featArray));
                    context.write(new IntWritable(centers.size()), new IntWritable(label));

                count++;   

             }

             newCenter.divide(count);
             centers.add(newCenter);
            if(newCenter.caculateErr(key) == 1)
                 context.getCounter(Counter.CONVERGED).increment(1);
        }
        @Override
          protected void cleanup(Context context) throws IOException,
              InterruptedException {
            super.cleanup(context);
            Configuration conf = context.getConfiguration();
            Path outPath = new Path(conf.get("centroid.path")+Integer.toString(Integer.parseInt(conf.get("iteration"))+1));
            FileSystem fs = FileSystem.get(conf);
            BufferedWriter bWriter = null;
            try{
                 bWriter = new BufferedWriter(new OutputStreamWriter(fs.create(outPath)));
            }catch(Exception e){
                e.printStackTrace();
            }  
            try{
                int number = 0;
                for (ClusterVector center:centers){
                    bWriter.write(number+" "+center.ToString());
                    bWriter.newLine();
                    number++;
                }
		bWriter.close();   
            }catch (Exception e) {
                 e.printStackTrace();
            }

    }
        }
     public static void runClassifier() throws Exception {

        long counter = 1;
        int iteration = 0;
        int featureNum = 17;
        int maxIteration = 20;
        int iterCounter = 0;
             while(counter > 0 && iterCounter < maxIteration){
            Configuration conf = new Configuration();
            conf.set("iteration", iteration + "");

            Path in = new Path("/user/shenh10/classifierInput");
            Path center = new Path("/user/shenh10/classifierCenter/centroid");
            conf.set("centroid.path", center.toString());
             conf.set("featureNum", featureNum+"");
            Path out = new Path("/user/shenh10/classifierOutput");

            Job job = Job.getInstance(conf, "KMeans-Classifier");
            job.setJarByClass(KMeans.class);
            job.setMapperClass(ClassifyMapper.class);
            job.setReducerClass(ClassifyReducer.class);
            job.setOutputKeyClass(ClusterVector.class);
            job.setOutputValueClass(Text.class);

            FileInputFormat.addInputPath(job, in);
            FileOutputFormat.setOutputPath(job, out);
           

            FileSystem fs = FileSystem.get(conf);
            if (fs.exists(out)) {
              fs.delete(out, true);
            }        
            job.waitForCompletion(true);
            iteration++;
            iterCounter ++;
            counter = job.getCounters().findCounter(ClassifyReducer.Counter.CONVERGED).getValue();
           //counter = 0;
        }  

     }
     public static void runSIFT()  throws Exception {
         long counter = 1;
        int iteration = 9;
        int featureNum = 1000;
        int maxIteration = 3;
        int iterCounter = 0;
        while(counter > 0 && iterCounter < maxIteration){
            Configuration conf = new Configuration();
            conf.set("iteration", iteration + "");

            Path in = new Path("/user/shenh10/siftInput");
            Path center = new Path("/user/shenh10/siftCenter/centroid");
            conf.set("centroid.path", center.toString());
             conf.set("featureNum", featureNum+"");
            Path out = new Path("/user/shenh10/siftOutput");

            Job job = Job.getInstance(conf, "KMeans-SIFT");
            job.setJarByClass(KMeans.class);
            job.setMapperClass(SIFTMapper.class);
            job.setReducerClass(SIFTReducer.class);
            job.setOutputKeyClass(ClusterVector.class);
            job.setOutputValueClass(Text.class);

            FileInputFormat.addInputPath(job, in);
            FileOutputFormat.setOutputPath(job, out);
           

            FileSystem fs = FileSystem.get(conf);
            if (fs.exists(out)) {
              fs.delete(out, true);
            }        
            job.waitForCompletion(true);
            iteration++;
            iterCounter++;
            counter = job.getCounters().findCounter(SIFTReducer.Counter.CONVERGED).getValue();
           //counter = 0;
        }
     }
     
    public static void runBOWProducer() throws Exception {
            int featureNum = 1000;
            Configuration conf = new Configuration();
            conf.set("featureNum", featureNum+"");
            conf.set("mapred.textoutputformat.separator", " ");
            Path in = new Path("/user/shenh10/bpInput");
            Path out = new Path("/user/shenh10/bpOutput");

            Job job = Job.getInstance(conf, "KMeans-BowProducer");
            job.setJarByClass(KMeans.class);
            job.setMapperClass(BOWProducerMapper.class);
            job.setReducerClass(BOWProducerReducer.class);
            job.setOutputKeyClass(IntWritable.class);
            job.setOutputValueClass(IntWritable.class);

            FileInputFormat.addInputPath(job, in);
            FileOutputFormat.setOutputPath(job, out);
           

            FileSystem fs = FileSystem.get(conf);
            if (fs.exists(out)) {
              fs.delete(out, true);
            }      
            
            job.waitForCompletion(true);

    }
    public static void main(String[] args) throws Exception 
    {
        int firstArg = 0;
      if (args.length > 0) {
            try {
                firstArg = Integer.parseInt(args[0]);
                if(firstArg == 0){
                    runSIFT();
                }
                else if (firstArg == 1){
                    runBOWProducer();
                }else if(firstArg == 2){
                    runClassifier();
                }
            } catch (NumberFormatException e) {
                System.err.println("Argument" + args[0] + " must be an integer.");
                System.exit(1);
            }
        }else {
            System.out.println("Please choose operation you take: 0-run SIFT only; 1-run BOW-Producer only; 2-run Classifier only ");
            return;
        }
        
    }
}
